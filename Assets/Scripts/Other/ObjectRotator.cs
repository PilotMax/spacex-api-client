﻿using UnityEngine;

namespace SpaceXClient
{
    public class ObjectRotator : MonoBehaviour
    {

        [SerializeField] private float _speed = 5.0f;
        [SerializeField] private Vector3 _params = Vector3.back;

        private void OnEnable()
        {
            transform.localRotation = Quaternion.identity;
        }
        void Update()
        {
            transform.localRotation = Quaternion.AngleAxis(Time.deltaTime * _speed, _params) * transform.localRotation;
        }
    }

}
