﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpaceXClient
{
	public class ErrorScreen : MonoBehaviour {

	public void RestartApp()
	{
		SceneManager.LoadScene(0);
	}
}

}
