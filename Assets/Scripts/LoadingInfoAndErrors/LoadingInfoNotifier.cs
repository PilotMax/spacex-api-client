﻿using UnityEngine;

namespace SpaceXClient
{
    public class LoadingInfoNotifier : MonoBehaviour
    {

        [SerializeField] private GameObject _loadingScreen;
        [SerializeField] private GameObject _connectionErrorScreen;

        private void ShowLoadingScreen()
        {
            _loadingScreen.SetActive(true);
        }
        private void HideLoadingScreen()
        {
            _loadingScreen.SetActive(false);
        }

        private void ShowConnectionErrorScreen()
        {
            HideLoadingScreen();
            _connectionErrorScreen.SetActive(true);
        }



        private void OnEnable() {
            EventsHandler.StartListening(GameEvent.ShowLoadingScreen, ShowLoadingScreen);
            EventsHandler.StartListening(GameEvent.HideLoadingScreen, HideLoadingScreen);
            EventsHandler.StartListening(GameEvent.ShowConnectionErrorScreen, ShowConnectionErrorScreen);
        }

        private void OnDisable() {
            EventsHandler.StopListening(GameEvent.ShowLoadingScreen, ShowLoadingScreen);
            EventsHandler.StopListening(GameEvent.HideLoadingScreen, HideLoadingScreen);
            EventsHandler.StopListening(GameEvent.ShowConnectionErrorScreen, ShowConnectionErrorScreen);
        }
    }
}
