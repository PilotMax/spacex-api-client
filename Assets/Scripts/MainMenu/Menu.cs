﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpaceXClient
{
    public class Menu : MonoBehaviour
    {
        private Animator _anim;
        private int _switchToRoadster = Animator.StringToHash("toRoadster");
        private int _switchToLaunches = Animator.StringToHash("toLaunches");
        private int _switchToMain = Animator.StringToHash("toMain");

        private void Awake()
        {
            _anim = GetComponent<Animator>();
        }

        public void SwitchToRoadsterSimulation()
        {
            _anim.SetTrigger(_switchToRoadster);
        }

        public void SwitchToLaunchesList()
        {
            _anim.SetTrigger(_switchToLaunches);
        }

        public void SwitchToMain()
        {
            _anim.SetTrigger(_switchToMain);
        }

        public void LoadSimulationScene()
        {
            SceneManager.LoadScene("RoadsterSimulation");
        }
    }

}
