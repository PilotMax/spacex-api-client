using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SpaceXClient
{
    public class EventsHandler : MonoBehaviour
    {

        private Dictionary<GameEvent, UnityEvent> eventDictionary;

        private static EventsHandler eventManager;

        public static EventsHandler Instance
        {
            get
            {
                if (eventManager == false)
                {
                    eventManager = FindObjectOfType(typeof(EventsHandler)) as EventsHandler;
                    if (eventManager == null)
                    {
                        Debug.Log("There needs to be one active EventManager on a GameObject in your scene");
                    }
                    else
                    {
                        eventManager.Init();
                    }
                }
                return eventManager;
            }
        }

        private void Init()
        {
            if (eventDictionary == null)
            {
                eventDictionary = new Dictionary<GameEvent, UnityEvent>();
            }
        }

        public static void StartListening(GameEvent eventName, UnityAction listener)
        {
            UnityEvent thisEvent = null;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.AddListener(listener);
            }
            else
            {
                thisEvent = new UnityEvent();
                thisEvent.AddListener(listener);
                Instance.eventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StopListening(GameEvent eventName, UnityAction listener)
        {
            if (eventManager == null)
            {
                return;
            }
            UnityEvent thisEvent = null;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.RemoveListener(listener);
            }
        }

        public static void TriggerEvent(GameEvent eventName)
        {
            UnityEvent thisEvent = null;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent))
            {
                thisEvent.Invoke();
                Debug.Log("Event was triggered: " + eventName);
            }
        }


    }
    public enum GameEvent
    {
        ShowLoadingScreen,
        HideLoadingScreen,
        ShowConnectionErrorScreen,
    }
}
