﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace SpaceXClient
{
    public class ShipsInfoPopup : MonoBehaviour
    {
        [SerializeField] private GameObject _shipInfoSlotPrefab;
        [SerializeField] private GameObject _noInfoSlotPrefab;
        [SerializeField] private RectTransform _slotsParent;
        [SerializeField] private GameObject _darkening;
        [SerializeField] private CanvasGroup _popupCanvasGroup;
        private List<ShipSlotUI> _shipsSlots = new List<ShipSlotUI>();
        private GameObject _noInfoSlot;
        const string _getShipDataRequest = "https://api.spacexdata.com/v3/ships/";

        public void SetupPopup(List<string> shipsIds)
        {
            DeactivateShipsInfoSlots();
            _darkening.SetActive(true);

            if (shipsIds == null || shipsIds.Count == 0)
            {
                ShowNoInfoSlot(true);
                return;
            }

            _popupCanvasGroup.alpha = 0;
            ShowNoInfoSlot(false);
            StartCoroutine(GetAllShipsData(shipsIds));

        }

        private void InstantiateSlots(List<ShipData> shipsData)
        {
            InstantiateNewSlots(shipsData);
            SetupSlots(shipsData);
        }

        private void DeactivateShipsInfoSlots()
        {
            for (int i = 0; i < _shipsSlots.Count; i++)
            {
                _shipsSlots[i].gameObject.SetActive(false);
            }
        }

        private void SetupSlots(List<ShipData> shipsData)
        {
            for (int i = 0; i < shipsData.Count; i++)
            {
                _shipsSlots[i].SetupSlot(shipsData[i]);
                _shipsSlots[i].gameObject.SetActive(true);
            }
        }

        private void InstantiateNewSlots(List<ShipData> shipsData)
        {
            if (shipsData.Count > _shipsSlots.Count)
            {
                for (int i = _shipsSlots.Count; i < shipsData.Count; i++)
                {
                    GameObject slot = Instantiate(_shipInfoSlotPrefab, _slotsParent);
                    _shipsSlots.Add(slot.GetComponent<ShipSlotUI>());
                }
            }
        }

        private IEnumerator GetAllShipsData(List<string> shipsIds)
        {
            EventsHandler.TriggerEvent(GameEvent.ShowLoadingScreen);

            List<ShipData> shipsData = new List<ShipData>();
            for (int i = 0; i < shipsIds.Count; i++)
            {
                string reqUrl = _getShipDataRequest + shipsIds[i].ToUpper();
                yield return StartCoroutine(GetShipData(reqUrl, data => shipsData.Add(data)));
            }
            InstantiateSlots(shipsData);

            EventsHandler.TriggerEvent(GameEvent.HideLoadingScreen);
            _popupCanvasGroup.alpha = 1;
        }

        private IEnumerator GetShipData(string url, System.Action<ShipData> shipDataCallback)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    EventsHandler.TriggerEvent(GameEvent.ShowConnectionErrorScreen);
                }
                else
                {
                    if (www.isDone)
                    {
                        string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                        ShipData shipData = JsonUtility.FromJson<ShipData>(jsonResult);
                        shipDataCallback(shipData);
                    }
                }
            }
        }

        private void ShowNoInfoSlot(bool show)
        {
            if (_noInfoSlot == null && show == false)
            {
                return;
            }
            else if (_noInfoSlot == null)
            {
                _noInfoSlot = Instantiate(_noInfoSlotPrefab, _slotsParent);
            }

            _noInfoSlot.SetActive(show);
        }

        public void HidePopup()
        {
            this.gameObject.SetActive(false);
            _darkening.SetActive(false);
        }


    }

}
