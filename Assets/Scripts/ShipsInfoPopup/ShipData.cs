﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SpaceXClient
{
	[Serializable]
    public class Position
    {
        public double latitude;
        public double longitude;
    }
	[Serializable]
    public class Mission
    {
        public string name;
        public int flight;
    }
	[Serializable]
    public class ShipData
    {
        public string ship_id;
        public string ship_name;
        public string ship_model;
        public string ship_type;
        public List<string> roles;
        public bool active;
        public int imo;
        public int mmsi;
        public int abs;
        public int @class;
        public int weight_lbs;
        public int weight_kg;
        public int year_built;
        public string home_port;
        public string status;
        public int speed_kn;
        public string course_deg;
        public Position position;
        public string successful_landings;
        public string attempted_landings;
        public List<Mission> missions;
        public string url;
        public string image;
    }
}
