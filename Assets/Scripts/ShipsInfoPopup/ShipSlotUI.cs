﻿using System;
using UnityEngine;
using TMPro;

namespace SpaceXClient
{
    public class ShipSlotUI : MonoBehaviour
    {

        [SerializeField] private TextMeshProUGUI _shipName;
        [SerializeField] private TextMeshProUGUI _usedInMissions;
        [SerializeField] private TextMeshProUGUI _shipType;
        [SerializeField] private TextMeshProUGUI _homePort;

        [Header("Ship Info text templates")]
        [SerializeField] private string _usedInMissionsTemplate;
        [SerializeField] private string _shipTypeTemplate;
        [SerializeField] private string _homePortTemplate;
        
        [Header("Open ship image")]
        [SerializeField] private UnityEngine.UI.Button _openImgUrlBtn;

        public void SetupSlot(ShipData data)
        {
            _shipName.text = data.ship_name;
            _usedInMissions.text = String.Format(_usedInMissionsTemplate,
                data.missions.Count.ToString());
            _shipType.text = String.Format(_shipTypeTemplate, data.ship_type);
            _homePort.text = String.Format(_homePortTemplate, data.home_port);
            _openImgUrlBtn.onClick.RemoveAllListeners();
            _openImgUrlBtn.onClick.AddListener(() => OpenUrl(data.image));
        }

        private void OpenUrl(string url)
        {
            Application.OpenURL(url);
        }
    }

}
