﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceXClient
{
    public class OrbitalData
    {

        //Epoch JD,Date UTC,Semi-major axis au,Eccentricity,Inclination degrees,Longitude of asc. node degrees,
        //Argument of periapsis degrees,Mean Anomaly degrees,True Anomaly degrees
        // Use this for initialization

        public double EpochJD;
        public DateTime DateUTC;
        public double SemiMajorAxisAu;
        public double Eccentricity;
        public double InclinationDegrees;
        public double LongitudeOfAcsNodeDegrees;
        public double ArgumentOfPeriapsisDegrees;
        public double MeanAnomalyDegrees;
        public double TrueAnomalyDegrees;
    }

}
