﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

namespace SpaceXClient
{
    public class SimulationUI : MonoBehaviour
    {
        [Header("Orbital text fields")]
        [SerializeField] private TextMeshProUGUI _epochJDText;
        [SerializeField] private TextMeshProUGUI _dateLocalText;
        [SerializeField] private TextMeshProUGUI _semiMajorAxisText;
        [SerializeField] private TextMeshProUGUI _eccentricityText;
        [SerializeField] private TextMeshProUGUI _inclinationText;
        [SerializeField] private TextMeshProUGUI _longitudeOfAcsNodeText;
        [SerializeField] private TextMeshProUGUI _argumentOfPeriapsisText;
        [SerializeField] private TextMeshProUGUI _meanAnomalyText;
        [SerializeField] private TextMeshProUGUI _trueAnomalyText;

        [Header("Orbital data templates")]
        [SerializeField] private string _epochJDTemplate;
        [SerializeField] private string _dateLocalTemplate;
        [SerializeField] private string _semiMajorAxisTemplate;
        [SerializeField] private string _eccentricityTemplate;
        [SerializeField] private string _inclinationTemplate;
        [SerializeField] private string _longitudeOfAcsNodeTemplate;
        [SerializeField] private string _argumentOfPeriapsisTemplate;
        [SerializeField] private string _meanAnomalyTemplate;
        [SerializeField] private string _trueAnomalyTemplate;

        public void UpdateOrbitalUI(OrbitalData data)
        {
            _epochJDText.text = System.String.Format(_epochJDTemplate, data.EpochJD);
            //TODO: NOT UTC, use local
            _dateLocalText.text = System.String.Format(_dateLocalTemplate, ConvertUTCToLocal(data.DateUTC));
            _semiMajorAxisText.text = System.String.Format(_semiMajorAxisTemplate, data.SemiMajorAxisAu);
            _eccentricityText.text = System.String.Format(_eccentricityTemplate, data.Eccentricity);
            _inclinationText.text = System.String.Format(_inclinationTemplate, data.InclinationDegrees);
            _longitudeOfAcsNodeText.text = System.String.Format(_longitudeOfAcsNodeTemplate, data.LongitudeOfAcsNodeDegrees);
            _argumentOfPeriapsisText.text = System.String.Format(_argumentOfPeriapsisTemplate, data.ArgumentOfPeriapsisDegrees);
            _meanAnomalyText.text = System.String.Format(_meanAnomalyTemplate, data.MeanAnomalyDegrees);
            _trueAnomalyText.text = System.String.Format(_trueAnomalyTemplate, data.TrueAnomalyDegrees);
        }

        private DateTime ConvertUTCToLocal(DateTime dateUTC)
        {
            try
            {
                TimeZoneInfo localZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneInfo.Local.Id);
                DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(dateUTC, localZone);
                Console.WriteLine("Converted {0} {1}.", dateUTC, localTime);
                return localTime;
            }
            catch (TimeZoneNotFoundException)
            {
                Console.WriteLine("The registry does not define the Central Standard Time zone.");
            }
            catch (InvalidTimeZoneException)
            {
                Console.WriteLine("Registry data on the Central Standard Time zone has been corrupted.");
            }

            return dateUTC;
        }

    }

}
