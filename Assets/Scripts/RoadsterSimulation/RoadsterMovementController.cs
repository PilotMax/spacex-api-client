﻿//using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RG.OrbitalElements;

namespace SpaceXClient
{
    public class RoadsterMovementController : MonoBehaviour
    {
        [SerializeField] private SimulationUI _simulationUI;
        [SerializeField] private LineRenderer _lineRenderer;
        private List<OrbitalData> _orbitalData;
        private Queue<Vector3> _cachedLastPositions = new Queue<Vector3>();
        private int _currentStep = 0;
        private int _maxSteps;
        private const float SCALE_ORBITAL_POSITION_BY = 1000.0f;
        private const int POSITIONS_TO_CACHE = 20;
        private const float MOVEMENT_UPDATE_TIME = 1;
        void Start()
        {
            _orbitalData = new LoadRoadsterOrbitalData().GetOrbitalData();
            _maxSteps = _orbitalData.Count;
            StartCoroutine(StartMovement());
        }

        IEnumerator StartMovement()
        {
            while (true)
            {
                transform.position = ConvertDoubleToFloatVector3(GetOrbitalPosition
                    (_orbitalData[_currentStep]), SCALE_ORBITAL_POSITION_BY);
                _simulationUI.UpdateOrbitalUI(_orbitalData[_currentStep]);

                if (_cachedLastPositions.Count > 0)
                {
                    _lineRenderer.positionCount = _cachedLastPositions.Count + 1;
                    _lineRenderer.SetPositions(_cachedLastPositions.ToArray());
                    _lineRenderer.SetPosition(_lineRenderer.positionCount - 1, transform.position);
                }

                if (_cachedLastPositions.Count < POSITIONS_TO_CACHE)
                {
                    _cachedLastPositions.Enqueue(transform.position);
                }
                else
                {
                    _cachedLastPositions.Dequeue();
                    _cachedLastPositions.Enqueue(transform.position);
                }

                _currentStep++;
                if (_currentStep >= _maxSteps)
                {
                    _currentStep = 0;
                }
                yield return new WaitForSeconds(MOVEMENT_UPDATE_TIME);
            }
        }

        private Vector3 ConvertDoubleToFloatVector3(Vector3Double doubleVec3, double divider)
        {
            return new Vector3((float)(doubleVec3.x / divider),
                            (float)(doubleVec3.y / divider),
                            (float)(doubleVec3.z / divider));
        }

        private Vector3Double GetOrbitalPosition(OrbitalData data)
        {
            return RG.OrbitalElements.Calculations.CalculateOrbitalPosition(
                semimajorAxis: data.SemiMajorAxisAu,
                eccentricity: data.Eccentricity,
                inclination: data.InclinationDegrees,
                longitudeOfAscendingNode: data.LongitudeOfAcsNodeDegrees,
                periapsisArgument: data.ArgumentOfPeriapsisDegrees,
                trueAnomaly: data.TrueAnomalyDegrees);
        }

        private void OnDrawGizmosSelected()
        {
            if (_orbitalData == null)
            {
                return;
            }
            for (int i = 0; i < _orbitalData.Count - 1; i++)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(ConvertDoubleToFloatVector3(GetOrbitalPosition(_orbitalData[i]), SCALE_ORBITAL_POSITION_BY),
                ConvertDoubleToFloatVector3(GetOrbitalPosition(_orbitalData[i + 1]), SCALE_ORBITAL_POSITION_BY));
            }
        }


    }
}

