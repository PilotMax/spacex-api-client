﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SpaceXClient
{
    public class SimulationMenu : MonoBehaviour
    {
        private Animator _anim;
        private int _switchToMain = Animator.StringToHash("toMain");

        private void Awake()
        {
            _anim = GetComponent<Animator>();
        }

        public void SwitchToMain()
        {
            _anim.SetTrigger(_switchToMain);
        }

        public void LoadMainMenuScene()
        {
            SceneManager.LoadScene(0);
        }
    }

}
