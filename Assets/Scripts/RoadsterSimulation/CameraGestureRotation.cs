﻿using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpaceXClient
{
    public class CameraGestureRotation : MonoBehaviour
 {
    [SerializeField] private float _rotSpeed = 0.5f;
    [SerializeField] private float _dir = -1;

    private float _rotX = 0f;
    private float _rotY  = 0f;
    private Vector3 _originRot;
    private Touch _initTouch;

     private void Start() {
        _originRot = transform.eulerAngles;
        _rotX = _originRot.x;
        _rotY = _originRot.y;
    }

    private void FixedUpdate() {
        foreach(Touch touch in Input.touches)
        {
            if(touch.phase == TouchPhase.Began)
            {
                _initTouch = touch;
            }
            else if(touch.phase == TouchPhase.Moved)
            {
                float deltaX = _initTouch.position.x - touch.position.x;
                float deltaY = _initTouch.position.y - touch.position.y;
                _rotX -= deltaY * Time.deltaTime * _rotSpeed * _dir;
                _rotY += deltaX * Time.deltaTime * _rotSpeed * _dir;
                transform.eulerAngles = new Vector3(_rotX, _rotY, 0f);
            }
            else if(touch.phase == TouchPhase.Ended)
            {
                _initTouch = new Touch();
            }
        }
    }

    public void ResetCameraRotation()
    {
        transform.rotation = Quaternion.Euler(_originRot.x, _originRot.y, 0);
        _rotX = _originRot.x;
        _rotY = _originRot.y;
    }
}

}
