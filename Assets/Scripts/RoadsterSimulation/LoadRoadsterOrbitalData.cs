﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace SpaceXClient
{
    public class LoadRoadsterOrbitalData
    {
        private List<OrbitalData> _orbitalData = new List<OrbitalData>();
        readonly DateTime _dateLimit = new DateTime(2019, 10, 8);

        public List<OrbitalData> GetOrbitalData()
        {
            TextAsset OrbitalDataFile = Resources.Load<TextAsset>("Roadster orbital elements JPL Horizon");
            string[] data = OrbitalDataFile.text.Split(new char[] { '\n' });
            for (int i = 1; i < data.Length - 1; i++)
            {
                string[] row = data[i].Split(new char[] { ',' });
                OrbitalData od = new OrbitalData();

                System.DateTime.TryParseExact(row[1], "yyyy-MM-dd HH:mm:ss",
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.None, out od.DateUTC);
                //08.10.2019
                if (DateTime.Compare(od.DateUTC, _dateLimit) > 0)
                {
                    break;
                }

                double.TryParse(row[0], NumberStyles.Any, CultureInfo.InvariantCulture, out od.EpochJD);
                double.TryParse(row[2], NumberStyles.Any, CultureInfo.InvariantCulture, out od.SemiMajorAxisAu);
                double.TryParse(row[3], NumberStyles.Any, CultureInfo.InvariantCulture, out od.Eccentricity);
                double.TryParse(row[4], NumberStyles.Any, CultureInfo.InvariantCulture, out od.InclinationDegrees);
                double.TryParse(row[5], NumberStyles.Any, CultureInfo.InvariantCulture, out od.LongitudeOfAcsNodeDegrees);
                double.TryParse(row[6], NumberStyles.Any, CultureInfo.InvariantCulture, out od.ArgumentOfPeriapsisDegrees);
                double.TryParse(row[7], NumberStyles.Any, CultureInfo.InvariantCulture, out od.MeanAnomalyDegrees);
                double.TryParse(row[8], NumberStyles.Any, CultureInfo.InvariantCulture, out od.TrueAnomalyDegrees);

                _orbitalData.Add(od);
            }

            return _orbitalData;
        }
    }

}
