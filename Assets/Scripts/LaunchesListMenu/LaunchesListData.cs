﻿using System.Collections.Generic;
using Rocket = SpaceXClient.RocketsParsed.Rocket;

namespace SpaceXClient
{
    public class LaunchesListData : ILaunchesListData
    {
        public List<LaunchesParsed.Launch> Launches { get; set; }
        public Dictionary<string, Rocket> RocketsIdCache { get; set; }
    }
}
