﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Rocket = SpaceXClient.RocketsParsed.Rocket;

namespace SpaceXClient
{
    public class LaunchesMenu : MonoBehaviour
    {
        [SerializeField] private RectTransform _slotsParent;
        [SerializeField] private GameObject _launchSlotPrefab;
        [SerializeField] private GameObject _shipsInfoPopupPrefab;
        [SerializeField] private Canvas _launchesCanvas;
        private ShipsInfoPopup _shipsInfoPopup;
        static string LAUNCHES_URL = "https://api.spacexdata.com/v3/launches";
        static string ROCKETS_URL = "https://api.spacexdata.com/v3/rockets";
        static string LAUNCHES_JSON_WRAPPER_KEY = "launches";
        static string ROCKETS_JSON_WRAPPER_KEY = "rockets";

        private void Start()
        {
            StartCoroutine(GetDataForLaunchesList(SpawnLaunchesListSlots));
        }

        private IEnumerator GetDataForLaunchesList(System.Action<ILaunchesListData> callback)
        {
            EventsHandler.TriggerEvent(GameEvent.ShowLoadingScreen);

            Dictionary<string, Rocket> rocketsIdCache = new Dictionary<string, Rocket>();
            yield return StartCoroutine(LaunchesMenuHelper.GetDataFromServer(ROCKETS_URL, data =>
            {
                RocketsParsed.RocketsDB db = LaunchesMenuHelper.CheckData<RocketsParsed.RocketsDB>
                    (data, ROCKETS_JSON_WRAPPER_KEY);
                rocketsIdCache = LaunchesMenuHelper.CacheRockets(db);
            }
            ));
            if (rocketsIdCache == null)
            {
                EventsHandler.TriggerEvent(GameEvent.ShowConnectionErrorScreen);
                yield break;
            }

            LaunchesParsed.LaunchesDB launchesDB = new LaunchesParsed.LaunchesDB();
            yield return StartCoroutine(LaunchesMenuHelper.GetDataFromServer(LAUNCHES_URL, data =>
            {
                launchesDB = LaunchesMenuHelper.CheckData<LaunchesParsed.LaunchesDB>
                    (data, LAUNCHES_JSON_WRAPPER_KEY);
            }
            ));
            if (launchesDB == null)
            {
                EventsHandler.TriggerEvent(GameEvent.ShowConnectionErrorScreen);
                yield break;
            }
            callback(new LaunchesListData()
            {
                Launches = launchesDB.launches,
                RocketsIdCache = rocketsIdCache
            });

            EventsHandler.TriggerEvent(GameEvent.HideLoadingScreen);
        }

        private void SpawnLaunchesListSlots(ILaunchesListData launchesListData)
        {
            for (int i = 0; i < launchesListData.Launches.Count; i++)
            {
                LaunchesParsed.Launch launch = launchesListData.Launches[i];
                GameObject slot = Instantiate(_launchSlotPrefab, _slotsParent);
                slot.GetComponent<LaunchSlotUI>().SetupSlot(new LaunchSlotData()
                {
                    Launch = launch,
                    Rocket = launchesListData.RocketsIdCache[launch.rocket.rocket_id]
                },
                ShowShipsInfoPopup);
            }
        }
        
        private void ShowShipsInfoPopup(List<string> shipsIds)
        {
            if (_shipsInfoPopup == null)
            {
                _shipsInfoPopup = Instantiate(_shipsInfoPopupPrefab, _launchesCanvas.transform)
                    .GetComponent<ShipsInfoPopup>();
            }
            _shipsInfoPopup.gameObject.SetActive(true);
            _shipsInfoPopup.SetupPopup(shipsIds);
        }
    }

}
