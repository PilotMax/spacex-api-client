﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Rocket = SpaceXClient.RocketsParsed.Rocket;

namespace SpaceXClient
{
    public static class LaunchesMenuHelper
    {
        public static Dictionary<string, Rocket> CacheRockets(RocketsParsed.RocketsDB rocketsDB)
        {
            if(rocketsDB == null)
            {
                return null;
            }
            Dictionary<string, Rocket> rocketsCache = new Dictionary<string, Rocket>();
            for (int i = 0; i < rocketsDB.rockets.Count; i++)
            {
                Rocket rocket;
                if (rocketsCache.TryGetValue(rocketsDB.rockets[i].rocket_id, out rocket) == false)
                {
                    rocketsCache.Add(rocketsDB.rockets[i].rocket_id, rocketsDB.rockets[i]);
                }
            }
            return rocketsCache;
        }

        public static string FormatJsonResult(byte[] data, string jsonWrapperKey)
        {
            string jsonResult = System.Text.Encoding.UTF8.GetString(data);
            jsonResult = System.String.Format("{0}{1}{2}{3}{4}", "{\"", jsonWrapperKey, "\":", jsonResult, "}");
            return jsonResult;
        }
        public static T CheckData<T>(byte[] data, string jsonWrapperKey)
        {
            if (data == null)
            {
                return default(T);
            }
            else
            {
                string jsonResult = FormatJsonResult(data, jsonWrapperKey);
                T db = JsonUtility.FromJson<T>(jsonResult);
                return db;
            }
        }
        public static IEnumerator GetDataFromServer(string url, System.Action<byte[]> callback)
        {
            using (UnityEngine.Networking.UnityWebRequest www =
                UnityEngine.Networking.UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    callback(null);
                }
                else
                {
                    if (www.isDone)
                    {
                        callback(www.downloadHandler.data);
                    }
                }
            }
        }
    }

}
