﻿namespace SpaceXClient
{
    public interface ILaunchSlotData
    {
        LaunchesParsed.Launch Launch { get; }
        RocketsParsed.Rocket Rocket { get; }
    }

}
