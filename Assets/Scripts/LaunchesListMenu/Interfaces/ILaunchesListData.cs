﻿using System.Collections.Generic;
using Rocket = SpaceXClient.RocketsParsed.Rocket;

namespace SpaceXClient
{
	public interface ILaunchesListData
    {
        List<LaunchesParsed.Launch> Launches { get; }
        Dictionary<string, Rocket> RocketsIdCache { get; }
    }
}
