﻿namespace SpaceXClient
{

    public class LaunchSlotData : ILaunchSlotData
    {
        public LaunchesParsed.Launch Launch {get;set;}
        public RocketsParsed.Rocket Rocket {get; set;}
    }

}
