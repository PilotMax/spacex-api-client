﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SpaceXClient.LaunchesParsed
{
    [Serializable]
    public class Core
    {
        public string core_serial;
        public int flight;
        public string block;
        public bool gridfins;
        public bool legs;
        public bool reused;
        public string land_success;
        public bool landing_intent;
        public string landing_type;
        public string landing_vehicle;
    }
    [Serializable]
    public class FirstStage
    {
        public List<Core> cores;
    }
    [Serializable]
    public class OrbitParams
    {
        public string reference_system;
        public string regime;
        public string longitude;
        public string semi_major_axis_km;
        public string eccentricity;
        public int periapsis_km;
        public int apoapsis_km;
        public int inclination_deg;
        public string period_min;
        public string lifespan_years;
        public string epoch;
        public string mean_motion;
        public string raan;
        public string arg_of_pericenter;
        public string mean_anomaly;
    }
    [Serializable]
    public class Payload
    {
        public string payload_id;
        public List<string> norad_id;
        public bool reused;
        public List<string> customers;
        public string nationality;
        public string manufacturer;
        public string payload_type;
        public int payload_mass_kg;
        public int payload_mass_lbs;
        public string orbit;
        public OrbitParams orbit_params;
    }
    [Serializable]
    public class SecondStage
    {
        public int block;
        public List<Payload> payloads;
    }
    [Serializable]
    public class Fairings
    {
        public bool reused;
        public bool recovery_attempt;
        public bool recovered;
        public string ship;
    }
    [Serializable]
    public class Rocket
    {
        public string rocket_id;
        public string rocket_name;
        public string rocket_type;
        public FirstStage first_stage;
        public SecondStage second_stage;
        public Fairings fairings;
    }
    [Serializable]
    public class Telemetry
    {
        public string flight_club;
    }
    [Serializable]
    public class LaunchSite
    {
        public string site_id;
        public string site_name;
        public string site_name_long;
    }
    [Serializable]
    public class LaunchFailureDetails
    {
        public int time;
        public string altitude;
        public string reason;
    }
    [Serializable]
    public class Links
    {
        public string mission_patch;
        public string mission_patch_small;
        public string reddit_campaign;
        public string reddit_launch;
        public string reddit_recovery;
        public string reddit_media;
        public string presskit;
        public string article_link;
        public string wikipedia;
        public string video_link;
        public string youtube_id;
        public List<string> flickr_images;
    }
    [Serializable]
    public class Timeline
    {
        public int webcast_liftoff;
    }

    [Serializable]
    public class Launch
    {
        public int flight_number;
        public string mission_name;
        public List<string> mission_id;
        public bool upcoming;
        public string launch_year;
        public int launch_date_unix;
        public string launch_date_utc;
        public string launch_date_local;
        public bool is_tentative;
        public string tentative_max_precision;
        public bool tbd;
        public int launch_window;
        public Rocket rocket;
        public List<string> ships;
        public Telemetry telemetry;
        public LaunchSite launch_site;
        public bool launch_success;
        public LaunchFailureDetails launch_failure_details;
        public Links links;
        public string details;
        public string static_fire_date_utc;
        public int static_fire_date_unix;
        public Timeline timeline;
    }

	[Serializable]
	public class LaunchesDB
	{
		public List<Launch> launches;
	}

}
