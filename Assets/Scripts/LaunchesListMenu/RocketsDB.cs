﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SpaceXClient.RocketsParsed
{
    [Serializable]
    public class Height
    {
        public double meters;
        public int feet;
    }
    [Serializable]
    public class Diameter
    {
        public double meters;
        public double feet;
    }
    [Serializable]
    public class Mass
    {
        public int kg;
        public int lb;
    }
    [Serializable]
    public class PayloadWeight
    {
        public string id;
        public string name;
        public int kg;
        public int lb;
    }
    [Serializable]
    public class ThrustSeaLevel
    {
        public int kN;
        public int lbf;
    }
    [Serializable]
    public class ThrustVacuum
    {
        public int kN;
        public int lbf;
    }
    [Serializable]
    public class FirstStage
    {
        public bool reusable;
        public int engines;
        public double fuel_amount_tons;
        public int burn_time_sec;
        public ThrustSeaLevel thrust_sea_level;
        public ThrustVacuum thrust_vacuum;
    }
    [Serializable]
    public class Thrust
    {
        public int kN;
        public int lbf;
    }
    [Serializable]
    public class Height2
    {
        public double meters;
        public double feet;
    }
    [Serializable]
    public class Diameter2
    {
        public double meters;
        public double feet;
    }
    [Serializable]
    public class CompositeFairing
    {
        public Height2 height;
        public Diameter2 diameter;
    }
    [Serializable]
    public class Payloads
    {
        public string option_1;
        public CompositeFairing composite_fairing;
    }
    [Serializable]
    public class SecondStage
    {
        public bool reusable;
        public int engines;
        public double fuel_amount_tons;
        public int burn_time_sec;
        public Thrust thrust;
        public Payloads payloads;
    }
    [Serializable]
    public class ThrustSeaLevel2
    {
        public int kN;
        public int lbf;
    }
    [Serializable]
    public class ThrustVacuum2
    {
        public int kN;
        public int lbf;
    }
    [Serializable]
    public class Engines
    {
        public int number;
        public string type;
        public string version;
        public string layout;
        public int engine_loss_max;
        public string propellant_1;
        public string propellant_2;
        public ThrustSeaLevel2 thrust_sea_level;
        public ThrustVacuum2 thrust_vacuum;
        public int thrust_to_weight;
    }
    [Serializable]
    public class LandingLegs
    {
        public int number;
        public string material;
    }
    [Serializable]
    public class Rocket
    {
        public int id;
        public bool active;
        public int stages;
        public int boosters;
        public int cost_per_launch;
        public int success_rate_pct;
        public string first_flight;
        public string country;
        public string company;
        public Height height;
        public Diameter diameter;
        public Mass mass;
        public List<PayloadWeight> payload_weights;
        public FirstStage first_stage;
        public SecondStage second_stage;
        public Engines engines;
        public LandingLegs landing_legs;
        public List<string> flickr_images;
        public string wikipedia;
        public string description;
        public string rocket_id;
        public string rocket_name;
        public string rocket_type;
    }
    [Serializable]
    public class RocketsDB
    {
        public List<Rocket> rockets;
    }

}

