﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SpaceXClient
{
    public class LaunchSlotUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _missionNameText;
        [SerializeField] private TextMeshProUGUI _nOfPayloads;
        [SerializeField] private TextMeshProUGUI _rocketName;
        [SerializeField] private TextMeshProUGUI _rocketOriginCountry;
        [SerializeField] private Image _launchStatusImage;
        [SerializeField] private Sprite _futureLaunchIcon;
        [SerializeField] private Sprite _pastLaunchIcon;

        [Header("Text headers templates")]
        [SerializeField] private string _missionNameTemplate;
        [SerializeField] private string _nOfPayloadsTemplate;
        [SerializeField] private string _rocketNameTemplate;
        [SerializeField] private string _rocketOriginTemplate;

        private List<string> _ships;
        private UnityEngine.UI.Button _shipsInfoPopupBtn;

        private void Awake()
        {
            _shipsInfoPopupBtn = GetComponent<UnityEngine.UI.Button>();
        }
        public void SetupSlot(ILaunchSlotData slotData,
                            System.Action<List<string>> showPopup)
        {
            _missionNameText.text = System.String.Format(_missionNameTemplate, slotData.Launch.mission_name);
            _nOfPayloads.text = System.String.Format(_nOfPayloadsTemplate,
                slotData.Launch.rocket.second_stage.payloads.Count.ToString());
            _rocketName.text = System.String.Format(_rocketNameTemplate, slotData.Launch.rocket.rocket_name);
            _rocketOriginCountry.text = System.String.Format(_rocketOriginTemplate, slotData.Rocket.country);
            _launchStatusImage.sprite = slotData.Launch.upcoming ? _futureLaunchIcon : _pastLaunchIcon;
            _ships = slotData.Launch.ships;
            _shipsInfoPopupBtn.onClick.RemoveAllListeners();
            _shipsInfoPopupBtn.onClick.AddListener(() => showPopup(_ships));
        }
    }
}
